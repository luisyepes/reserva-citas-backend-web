package com.reservacitas.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table (name = "workday")
public class WorkDay extends Entities {

	/**
	 * a WorkDay is a day a specialist is available to attend customers
	 * @author yepes
	 */
	private static final long serialVersionUID = -2660913791520377203L;

	@NotNull
	@Column
	private String day;
	
	@NotNull
	@Column
	@DateTimeFormat (pattern = "yyyy-MM-dd HH:mm")
	private Date start;
	
	@NotNull
	@Column
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
	private Date end;
	
	public WorkDay() {
		super();
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}

}
