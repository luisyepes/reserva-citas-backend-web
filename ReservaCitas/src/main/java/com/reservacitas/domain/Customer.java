package com.reservacitas.domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table (name = "customer")
public class Customer extends Entities {

	/**
	 * Object that represents the client who is booking, not login required
	 * @author yepes
	 */

	private static final long serialVersionUID = 2554691445590083007L;
	
	@NotNull
	@Column (name = "name")
	@Size (max = 100)
	private String name;
	
	/* 
	 * identification could be an email, phone number, any other identification number or String
	 *  the business uses to know its customers
	 */
	@Column (name = "identification")
	@Size (max = 100)
	private String identification;
	
	@OneToMany (fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable (name = "customer_appointments",
				joinColumns = {@JoinColumn(name="customer_id", referencedColumnName = "id")},
				inverseJoinColumns = {@JoinColumn(name = "appointment_id", referencedColumnName = "id")})
	private List<Appointment> appointments;

	public Customer() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIdentification() {
		return identification;
	}

	public void setIdentification(String identification) {
		this.identification = identification;
	}

	public List<Appointment> getAppointments() {
		return appointments;
	}

	public void setAppointments(List<Appointment> appointments) {
		this.appointments = appointments;
	}

}
