package com.reservacitas.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table (name = "service")
public class Service extends Entities {

	/**
	 * The service is the work the specialist can help the customer with
	 * @author yepes
	 */
	
	private static final long serialVersionUID = 5966400199564932333L;

	@NotNull
	@Column
	@Size (max = 100)
	private String name;
	
	@Column
	@Size (max = 500)
	private String description;
	
	@NotNull
	@DateTimeFormat (pattern = "HH:mm")
	@Column
	private Date duration;
		
	@Column
	@Size (max = 6)
	private double cost;
	
	@ManyToMany (fetch = FetchType.LAZY)
	@JoinTable (name = "service_specialists", 
				joinColumns = @JoinColumn(name = "service_id", referencedColumnName = "id"),
				inverseJoinColumns = @JoinColumn(name = "specialist_id", referencedColumnName = "id")
				)
	private List<Specialist> specialists;
		
	public Service() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDuration() {
		return duration;
	}

	public void setDuration(Date duration) {
		this.duration = duration;
	}

	public double getCost() {
		return cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}

	public List<Specialist> getSpecialists() {
		return specialists;
	}

	public void setSpecialists(List<Specialist> specialists) {
		this.specialists = specialists;
	}

}
