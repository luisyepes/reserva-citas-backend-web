package com.reservacitas.domain;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.springframework.data.domain.Persistable;

@MappedSuperclass
public abstract class Entities implements Persistable<Long> {

	/**
	 * Abstract class with default attributes for any entity
	 * @author yepes
	 * @version 1.0
	 */
		
	private static final long serialVersionUID = -1162107563166861610L;
	
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	@Column (name = "id")
	protected Long id;
	
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public Long getId() {
		return id;
	}
	
	@Override
	public boolean isNew() {
		return (getId() == null);
	}

}
