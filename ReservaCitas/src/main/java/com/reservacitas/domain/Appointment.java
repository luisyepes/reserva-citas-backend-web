package com.reservacitas.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table (name = "appointment")
public class Appointment extends Entities {

	/**
	 * this class is the object that represents a booking
	 * @author yepes
	 */
	private static final long serialVersionUID = -1571156384594687852L;

	@NotNull
	@DateTimeFormat (pattern = "yyyy-MM-dd HH:mm")
	@Column (name = "start")
	private Date start;
	
	@Column (name = "attended")
	private boolean attended; 
	
	@ManyToOne(optional = false)
	@JoinColumn
	private Service service;
	
	public Appointment() {
		super();
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public boolean isAttended() {
		return attended;
	}

	public void setAttended(boolean attended) {
		this.attended = attended;
	}

	public Service getService() {
		return service;
	}

	public void setService(Service service) {
		this.service = service;
	}

}
