package com.reservacitas.domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table (name = "company")
public class Company extends Entities {

	/**
	 * This is an object that represents the business the specialists belong to
	 * it is instanced just once at the app first set up
	 * @author yepes
	 */
	private static final long serialVersionUID = -3988523718518517103L;

	@NotNull
	@Column (name = "name")
	@Size (max = 100)
	private String name;
	
	@NotNull
	@Column (name = "phone_number")
	@Size (max = 20)
	private int phoneNumber;
	
	@NotNull
	@Column (name = "logo_url")
	@Size (max = 200)
	private String logoURL;
	
	@NotNull
	@Column (name = "address")
	@Size (max = 100)
	private String address;
	
	@Column (name = "longitude")
	@Size (max = 200)
	private double longitude;
	
	@Column (name = "latitude")
	@Size (max = 200)
	private double latitude; 
	
	@OneToMany (cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinTable (name = "company_specialists",
				joinColumns = {@JoinColumn(name = "company_id", referencedColumnName="id")},
				inverseJoinColumns = {@JoinColumn(name = "specialist_id", referencedColumnName="id")}
				)
	private List<Specialist> specialists;
	
	@OneToMany (fetch = FetchType.LAZY)
	@JoinTable (name = "company_customers",
				joinColumns = {@JoinColumn(name = "company_id", referencedColumnName="id")},
				inverseJoinColumns = {@JoinColumn(name = "customer_id", referencedColumnName="id")}
				)
	private List<Customer> customers;	
	
	public Company() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(int phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getLogoURL() {
		return logoURL;
	}

	public void setLogoURL(String logoURL) {
		this.logoURL = logoURL;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public List<Specialist> getSpecialists() {
		return specialists;
	}

	public void setSpecialists(List<Specialist> specialists) {
		this.specialists = specialists;
	}

	public List<Customer> getCustomers() {
		return customers;
	}

	public void setCustomers(List<Customer> customers) {
		this.customers = customers;
	}

}
