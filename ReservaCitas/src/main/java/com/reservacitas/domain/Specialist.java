package com.reservacitas.domain;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table (name = "specialist")
public class Specialist extends Entities {

	/**
	 * The specialist is the person the customer book an appointment with
	 * @author yepes
	 */
	
	private static final long serialVersionUID = -2131274760897536654L;

	@NotNull
	@Column
	@Size (max = 100)
	private String name;
	
	@Column
	@Size (max = 200)
	private String photoURL;
	
	/*
	 * the contact could be a phone number or email to get in contact with the specialist
	 */
	@Column
	@Size (max = 100)
	private String contact;
	
	@Column
	private boolean available;
	
	@OneToMany (fetch = FetchType.LAZY)
	@JoinTable(name = "specialist_workdays", 
			   joinColumns = @JoinColumn(name = "specialist_id", referencedColumnName = "id"),
			   inverseJoinColumns = @JoinColumn(name = "workDay_id", referencedColumnName = "id"))
	private List<WorkDay> workDays;
	
	@ManyToMany (fetch = FetchType.LAZY, mappedBy = "specialists", targetEntity = Service.class)
	private List<Service> Services;
	
	public Specialist() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhotoURL() {
		return photoURL;
	}

	public void setPhotoURL(String photoURL) {
		this.photoURL = photoURL;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}
	
	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}

	public List<WorkDay> getWorkDays() {
		return workDays;
	}

	public void setWorkDays(List<WorkDay> workDays) {
		this.workDays = workDays;
	}

	public List<Service> getServices() {
		return Services;
	}

	public void setServices(List<Service> services) {
		Services = services;
	}
	
}
